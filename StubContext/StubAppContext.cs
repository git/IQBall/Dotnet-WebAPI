using AppContext.Entities;
using DbServices;
using Microsoft.EntityFrameworkCore;
using Model;

namespace StubContext;

using AppContext;

public class StubAppContext(DbContextOptions<AppContext> options) : AppContext(options)
{
    public StubAppContext() : this(
        new DbContextOptionsBuilder<AppContext>()
            .UseSqlite("DataSource=database.db")
            .Options
    )
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        var users = new[] { "maxime", "mael", "yanis", "simon", "vivien" }.ToList();
        var i = 0;

        builder.Entity<UserEntity>()
            .HasData(users.ConvertAll(name =>
            {
                var (password, salt) = Hashing.HashString("123456");
                return new UserEntity
                {
                    Id = ++i,
                    Email = $"{name}@mail.com",
                    Name = name,
                    Password = password,
                    PasswordSalt = salt,
                    IsAdmin = true,
                    ProfilePicture =
                        "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png",
                };
            }));

        builder.Entity<TacticEntity>()
            .HasData(new TacticEntity
            {
                Id = 1,
                Name = "New tactic",
                Type = CourtType.Plain,
                CreationDate = new DateTime(2024, 5, 31),
                OwnerId = 1,
            });

        builder.Entity<TacticStepEntity>()
            .HasData(new TacticStepEntity
            {
                Id = 1,
                JsonContent = "{\"components\": []}",
                TacticId = 1,
                ParentId = null
            });
        
        builder.Entity<SharedTacticEntity>()
            .HasData(new SharedTacticEntity
            {
                Id = 1,
                TacticId = 1,
                SharedWithUserId = 2
            });


        builder.Entity<TeamEntity>()
            .HasData(new TeamEntity
            {
                Id = 1,
                Name = "Lakers",
                Picture =
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Los_Angeles_Lakers_logo.svg/2560px-Los_Angeles_Lakers_logo.svg.png",
                MainColor = "#FFFFFF",
                SecondColor = "#000000",
            });
        
        builder.Entity<TeamEntity>()
            .HasData(new TeamEntity
            {
                Id = 2,
                Name = "Auvergne",
                Picture =
                    "https://sancy.iut.uca.fr/~lafourcade/img/photo19.jpg",
                MainColor = "#FFFFFF",
                SecondColor = "#000000",
            });
        
        
        builder.Entity<MemberEntity>()
            .HasKey("TeamId", "UserId");


        builder.Entity<MemberEntity>()
            .HasData(
                new MemberEntity
                {
                    Role = MemberRole.Coach,
                    UserId = 1,
                    TeamId = 1
                }, new MemberEntity
                {
                    Role = MemberRole.Player,
                    UserId = 2,
                    TeamId = 1
                }
            );
    }
}