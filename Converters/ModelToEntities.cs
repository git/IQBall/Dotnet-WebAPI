﻿using AppContext.Entities;
using Microsoft.EntityFrameworkCore;
using Model;

namespace Converters;

public static class EntitiesToModels
{
    public static User ToModel(this UserEntity entity)
    {
        return new User(entity.Id, entity.Name, entity.Email, entity.ProfilePicture, entity.IsAdmin);
    }

    public static Tactic ToModel(this TacticEntity entity)
    {
        return new Tactic(entity.Id, entity.Name, entity.OwnerId, entity.Type, entity.CreationDate);
    }

    public static TacticStep ToModel(this TacticStepEntity entity, IQueryable<TacticStepEntity> steps)
    {
        return new TacticStep(
            entity.Id,
            entity.ParentId,
            steps.Where(s =>s.TacticId == entity.TacticId && s.ParentId == entity.Id)
                .ToList()
                .Select(e => e.ToModel(steps)),
            entity.JsonContent
        );
    }

    public static Team ToModel(this TeamEntity entity)
    {
        return new Team(entity.Id, entity.Name, entity.Picture, entity.MainColor, entity.SecondColor);
    }
    
    public static Member ToModel(this MemberEntity entity)
    {
        return new Member(entity.TeamId, entity.UserId, entity.Role);
    }
    
    public static SharedTactic ToModel(this SharedTacticEntity entity)
    {
        return new SharedTactic(entity.Id, entity.TacticId, entity.SharedWithUserId, entity.SharedWithTeamId);
    }
}