namespace Model;

public record TacticStep(int Id, int? Parent, IEnumerable<TacticStep> Children, string Content);