﻿namespace Model;

public record SharedTactic(int Id, int TacticId, int? SharedWithUserId, int? SharedWithTeamId);