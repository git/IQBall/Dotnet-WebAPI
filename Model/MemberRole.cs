namespace Model;

public enum MemberRole
{
    Player,
    Coach
}