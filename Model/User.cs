namespace Model;

public record User(int Id, string Name, string Email, string ProfilePicture, bool IsAdmin);