namespace Model;

public record Team(int Id, string Name, string Picture, string MainColor, string SecondColor);