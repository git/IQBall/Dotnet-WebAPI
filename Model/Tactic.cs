namespace Model;

public record Tactic(int Id, string Name, int OwnerId, CourtType CourtType, DateTime CreationDate);