namespace Model;

public record Member(int TeamId, int UserId, MemberRole Role);