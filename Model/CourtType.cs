using System.ComponentModel.DataAnnotations.Schema;

namespace Model;

public enum CourtType
{
    Plain,
    Half
}