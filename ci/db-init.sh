set -xeu

BRANCH=$1
COMMIT_SHA=$2

git clone https://codefirst.iut.uca.fr/git/IQBall/Dotnet-WebAPI

cd Dotnet-WebAPI

git switch $BRANCH

#TODO remove migrations that are bound to commits that are no longer in the branch (which could occur via a rebase)

cd AppContext
rm -rf Migrations # remove migrations from git
ln -s /migrations Migrations # bind auto branch migrations to the project

# apply migrations
dotnet ef migrations add "auto-migration-$COMMIT_SHA"

# update database
dotnet ef database update
