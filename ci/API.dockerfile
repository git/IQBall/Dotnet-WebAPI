FROM mcr.microsoft.com/dotnet/sdk:8.0 AS builder

WORKDIR /home/

ADD . .

RUN cd ./API && dotnet publish -c Release

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS runtime

WORKDIR /home/

COPY --from=builder /home/API/bin/Release/net8.0 /home/

#$PGSQL_DSN is an environment variable
ENTRYPOINT /home/API