set -xeu

BRANCH=$1

sudo docker build -t iqball-api-dotnet:$BRANCH -f ci/Dockerfile .
sudo docker tag iqball-api-dotnet:$BRANCH hub.codefirst.iut.uca.fr/maxime.batista/iqball-api-dotnet:$BRANCH
sudo docker login hub.codefirst.iut.uca.fr
sudo docker push hub.codefirst.iut.uca.fr/maxime.batista/iqball-api-dotnet:$BRANCH