
set -xeu

docker network create iqball_net

docker run -d \
  --name nginx-iqball-proxy \
  -v /srv/nginx/nginx-iqball-proxy.conf:/etc/nginx/conf.d/default.conf \
  -p 8081:8080 \
  --restart=always \
  --network iqball_net \
  nginx