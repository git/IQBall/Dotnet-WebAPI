#!/usr/bin/env bash

set -exu

BRANCH=$1
BRANCH_ESCAPED=$(echo $BRANCH | tr / _)
COMMIT_SHA=$2

API_CONTAINER_NAME="iqball-api-dotnet-$BRANCH_ESCAPED"
DB_CONTAINER_NAME="iqball-db-$BRANCH_ESCAPED"

(docker stop "$API_CONTAINER_NAME" && docker rm "$API_CONTAINER_NAME") || true
docker volume create "iqball-migrations-$BRANCH_ESCAPED" || true

docker run -d \
  --name "$DB_CONTAINER_NAME" \
  --restart=always \
  --network iqball_net \
  --env POSTGRES_PASSWORD=1234 \
  --env POSTGRES_USER=iqball \
  --env POSTGRES_DATABASE=iqball \
  postgres || true #run the database container if it does not already exists

# apply migrations on database
docker run --rm -t \
  --env PGSQL_DSN="Server=$DB_CONTAINER_NAME;Username=iqball;Password=1234;Database=iqball" \
  --env BRANCH="$BRANCH" \
  --env COMMIT_SHA="$COMMIT_SHA" \
  --mount source="iqball-migrations-$BRANCH_ESCAPED",target=/migrations \
  --network iqball_net \
  iqball-db-init:latest


docker pull "hub.codefirst.iut.uca.fr/maxime.batista/iqball-api-dotnet:$BRANCH_ESCAPED"

# run the API
docker run -d \
  --name "$API_CONTAINER_NAME" \
  --restart=always \
  --network iqball_net \
  --env PGSQL_DSN="Server=$DB_CONTAINER_NAME;Username=iqball;Password=1234;Database=iqball" \
  "hub.codefirst.iut.uca.fr/maxime.batista/iqball-api-dotnet:$BRANCH_ESCAPED"



