#!/usr/bin/env bash

set -exu

apt update && apt install rsync openssh-client -y

mkdir -p ~/.ssh
echo "$PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 0600 ~/.ssh
chmod 0500 ~/.ssh/id_rsa*
ssh -p 80 -o 'StrictHostKeyChecking=no' iqball@maxou.dev mkdir -p /srv/www/iqball/$DRONE_BRANCH/
rsync -avz -e "ssh -p 80 -o 'StrictHostKeyChecking=no'" ci/deploy_staging_server.sh iqball@maxou.dev:/srv/www/iqball/$DRONE_BRANCH/
ssh -p 80 -o 'StrictHostKeyChecking=no' iqball@maxou.dev "chmod +x /srv/www/iqball/$DRONE_BRANCH/deploy_staging_server.sh && /srv/www/iqball/$DRONE_BRANCH/deploy_staging_server.sh $DRONE_BRANCH $DRONE_COMMIT_SHA"
    