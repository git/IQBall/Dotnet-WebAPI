FROM mcr.microsoft.com/dotnet/sdk:8.0 


WORKDIR /home/

RUN apt update && apt install git -y

RUN dotnet tool install --global dotnet-ef
ENV PATH="$PATH:/root/.dotnet/tools"

ADD --chmod=755 db-init.sh ./

ENTRYPOINT ./db-init.sh $BRANCH $COMMIT_SHA

