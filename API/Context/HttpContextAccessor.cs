using API.Validation;

namespace API.Context;

public class HttpContextAccessor : IContextAccessor
{
    public int CurrentUserId(HttpContext ctx)
    {
        var idClaim = ctx
            .User
            .Claims
            .First(c => c.Type == IdentityData.IdUserClaimName);
        return int.Parse(idClaim.Value);
    }
}