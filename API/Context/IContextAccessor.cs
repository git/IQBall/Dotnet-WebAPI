namespace API.Context;

public interface IContextAccessor
{
    public int CurrentUserId(HttpContext ctx);
}