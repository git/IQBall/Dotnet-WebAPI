using Model;

namespace API.DTO;

public static class ModelToDto
{
    public static TacticDto ToDto(this Tactic t)
    {
        return new TacticDto(t.Id, t.Name, t.OwnerId, t.CourtType.ToString().ToUpper(), new DateTimeOffset(t.CreationDate).ToUnixTimeMilliseconds());
    }

    public static TacticStepDto ToDto(this TacticStep t)
    {
        return new TacticStepDto(t.Id, t.Parent, t.Children.Select(s => s.ToDto()));
    }
}