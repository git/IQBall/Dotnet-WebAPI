namespace API.DTO;

public record TacticStepDto(int Id, int? ParentId, IEnumerable<TacticStepDto> Children);