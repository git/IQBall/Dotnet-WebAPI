namespace API.DTO;

public record TacticDto(int Id, string Name, int OwnerId, string CourtType, long CreationDate);