using Model;

namespace API.DTO;

public record MemberDto(User User, MemberRole Role);