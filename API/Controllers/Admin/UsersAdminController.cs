using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Model;
using Services;

namespace API.Controllers.Admin;

/// <summary>
/// WARNING: This controller does not requires the requester to be authenticated, see https://codefirst.iut.uca.fr/git/IQBall/Server-Panel/issues/2
/// </summary>
/// <param name="service"></param>
[ApiController]
public class UsersAdminController(IUserService service, ILogger<UsersAdminController> logger) : ControllerBase
{
    public record CountUsersResponse(int Value);


    [HttpGet("/admin/users/count&search={search}")]
    public async Task<CountUsersResponse> CountUsers(
        [MaxLength(256, ErrorMessage = "Search string is too wide")]
        string search
    )
    {
        logger.LogTrace("Counting Users");
        return new CountUsersResponse(await service.UsersCount(search));
    }

    [HttpGet("/admin/users/count")]
    public async Task<CountUsersResponse> CountUsers()
    {
        logger.LogTrace("Counting Users");
        return new CountUsersResponse(await service.UsersCount());
    }
    

    [HttpGet("/admin/users")]
    public async Task<IEnumerable<User>> ListUsers(
        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        int start,
        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        int n,
        [MaxLength(256, ErrorMessage = "Search string is too wide")]
        string? search
    )
    {
        logger.LogTrace("Listing Users");

        var result = await service.ListUsers(start, n, search);
        return result;
    }

    [HttpGet("/admin/users/{id:int}")]
    public async Task<IActionResult> GetUser(
        [Range(1, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        int id
    )
    {
        logger.LogTrace("Getting a specific User ({})", id);

        var result = await service.GetUser(id);
        if (result == null)
            return NotFound();

        return Ok(result);
    }

    public record AddUserRequest(
        [MaxLength(256, ErrorMessage = "Username is too wide")]
        string Username,
        [Range(4, 256, ErrorMessage = "Password must length be between 4 and 256")]
        string Password,
        [MaxLength(256, ErrorMessage = "Email is too wide")]
        [EmailAddress]
        string Email,
        bool IsAdmin = false
    );

    [HttpPost("/admin/users")]
    public Task<User> AddUser([FromBody] AddUserRequest req)
    {
        logger.LogTrace("Adding a User");

        return service.CreateUser(req.Username, req.Email, req.Password, UsersController.DefaultProfilePicture,
            req.IsAdmin);
    }

    public record RemoveUsersRequest(int[] Identifiers);

    [HttpPost("/admin/users/remove-all")]
    public async Task<IActionResult> RemoveUsers([FromBody] RemoveUsersRequest req)
    {
        logger.LogTrace("Removing Users");

        await service.RemoveUsers(req.Identifiers);
        return Ok();
    }

    public record UpdateUserRequest(
        [MaxLength(256, ErrorMessage = "Username is too wide")]
        string Username,
        [MaxLength(256, ErrorMessage = "Email is too wide")]
        [EmailAddress]
        string Email,
        bool IsAdmin
    );

    [HttpPut("/admin/users/{id:int}")]
    public async Task<IActionResult> UpdateUser(
        int id,
        [FromBody] UpdateUserRequest req
    )
    {
        try
        {
            logger.LogTrace("Updating Users");

            await service.UpdateUser(new User(id, req.Username, req.Email, UsersController.DefaultProfilePicture,
                req.IsAdmin));
            return Ok();
        }
        catch (ServiceException e)
        {
            return BadRequest(e.FailuresMessages());
        }
    }
}