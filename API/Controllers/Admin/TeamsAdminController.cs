using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Model;
using Services;

namespace API.Controllers.Admin;


/// <summary>
/// WARNING: This controller does not requires the requester to be authenticated, see https://codefirst.iut.uca.fr/git/IQBall/Server-Panel/issues/2
/// </summary>
/// <param name="service"></param>
[ApiController]
public class TeamsAdminController(ITeamService service, ILogger<TeamsAdminController> logger) : ControllerBase
{
    public record CountTeamsResponse(int Value);


    [HttpGet("/admin/teams/count")]
    public async Task<CountTeamsResponse> CountTeams()
    {
        logger.LogTrace("Counting teams");
        return new CountTeamsResponse(await service.CountTotalTeams());
    }
    
    [HttpGet("/admin/teams")]
    public Task<IEnumerable<Team>> ListTeams(
        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        int start,
        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        int n
    )
    {
        logger.LogTrace("Listing teams");
        return service.ListTeams(start, n);
    }

    public record AddTeamRequest(string Name, string Picture, string FirstColor, string SecondColor);

    [HttpPost("/admin/teams")]
    public async Task<IActionResult> AddTeam([FromBody] AddTeamRequest req)
    {
        logger.LogTrace("Adding teams");
        var team = await service.AddTeam(req.Name, req.Picture, req.FirstColor, req.SecondColor);

        return Ok(team);
    }

    public record UpdateTeamRequest(string Name, string Picture, string MainColor, string SecondaryColor);

    [HttpPut("/admin/teams/{teamId:int}")]
    public async Task<IActionResult> UpdateTeam(int teamId, [FromBody] UpdateTeamRequest req)
    {
        logger.LogTrace("Updating teams");
        await service.UpdateTeam(new Team(teamId, req.Name, req.Picture, req.MainColor, req.SecondaryColor));

        return Ok();
    }

    public record DeleteTeamsRequest(int[] Teams);

    [HttpPost("/admin/teams/remove-all")]
    public async Task<IActionResult> DeleteTeams([FromBody] DeleteTeamsRequest req)
    {
        logger.LogTrace("Deleting teams");

        await service.RemoveTeams(req.Teams);
        return Ok();
    }
}