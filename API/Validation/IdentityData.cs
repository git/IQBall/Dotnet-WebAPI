using System.Runtime.CompilerServices;

[assembly:InternalsVisibleTo("UnitTests")]
namespace API.Validation;

internal class IdentityData
{
    public const string AdminUserClaimName = "admin";
    public const string AdminUserPolicyName = "Admin";
    
    public const string IdUserClaimName = "id";
}