using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace API.Validation;

public partial class NameAttribute : ValidationAttribute
{
    protected override ValidationResult? IsValid(object? value, ValidationContext context)
    {
        var name = context.DisplayName;

        if (value is null)
            return ValidationResult.Success;
        
        if (value is not string str)
        {
            return new ValidationResult($"{name} should be a string.");
        }

        if (NameRegex().Match(str).Success)
        {
            return ValidationResult.Success;
        }

        return new ValidationResult(
            $"{name} should only contain numbers, letters, accents, spaces, _ and - characters.");
    }

    [GeneratedRegex("^[0-9a-zA-Zà-üÀ-Ü _-]*$")]
    private static partial Regex NameRegex();
}