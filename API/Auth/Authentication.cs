using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.IdentityModel.Tokens;

namespace API.Auth;

public static class Authentication
{
    private static readonly TimeSpan TokenLifetime = TimeSpan.FromMinutes(15);

    public static (string, DateTime) GenerateJwt(SymmetricSecurityKey key, IEnumerable<Claim> claims)
    {
        var expirationDate = DateTime.UtcNow.Add(TokenLifetime);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(claims),
            Expires = expirationDate,
            SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
        };
        var tokenHandler = new JwtSecurityTokenHandler();

        var token = tokenHandler.CreateToken(tokenDescriptor);

        var jwt = tokenHandler.WriteToken(token);
        return ("Bearer " + jwt, expirationDate);
    }
    
}