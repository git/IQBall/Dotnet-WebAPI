﻿using Microsoft.EntityFrameworkCore;

namespace EFConsole;


class UsersConsole
{
    internal static void TestAddUser(AppContext.AppContext db)
    {
        var newUser = new AppContext.Entities.UserEntity
        {
            Name = "Pierre",
            Email = "pierre@mail.com",
            Password = "123456",
            PasswordSalt = [1],
            ProfilePicture = "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png",
            IsAdmin = false
        };
        
        db.Users.Add(newUser);
        db.SaveChanges();

        Console.WriteLine("Utilisateur ajouté avec succès !");
    }

    internal static void TestGetAllUsers(AppContext.AppContext db)
    {
        var users = db.Users.ToList();
        Console.WriteLine("Liste des utilisateurs :");
        foreach (var user in users)
        {
            Console.WriteLine($"ID : {user.Id}, Nom : {user.Name}, Email : {user.Email}");
        }
    }

    internal static void TestFindUserByMail(AppContext.AppContext db, string userEmail)
    {
        var userByEmail = db.Users.FirstOrDefault(u => u.Email == userEmail);
        if (userByEmail != null)
        {
            Console.WriteLine($"Utilisateur trouvé par email : {userByEmail.Name}");
        }
        else
        {
            Console.WriteLine($"Aucun utilisateur trouvé avec l'email : {userEmail}");
        }
    }

    internal static void TestUpdateUser(AppContext.AppContext db)
    {
        var userToUpdate = db.Users.FirstOrDefault(u => u.Name == "Pierre");
        if (userToUpdate != null)
        {
            userToUpdate.Name = "Paul";
            db.SaveChanges();
            Console.WriteLine("Utilisateur mis à jour avec succès !");
        }
        else
        {
            Console.WriteLine("Utilisateur non trouvé pour la mise à jour.");
        }
    }

    internal static void TestDeleteUser(AppContext.AppContext db)
    {
        var userToDelete = db.Users.FirstOrDefault(u => u.Name == "Paul");
        if (userToDelete != null)
        {
            db.Users.Remove(userToDelete);
            db.SaveChanges();
            Console.WriteLine("Utilisateur supprimé avec succès !");
        }
        else
        {
            Console.WriteLine("Utilisateur non trouvé pour la suppression.");
        }
    }

    internal static void TestSearchUsersByName(AppContext.AppContext db, string userName)
    {
        var usersByName = db.Users.Where(u => u.Name == userName).ToList();
        Console.WriteLine($"Utilisateurs avec le nom '{userName}' :");
        foreach (var user in usersByName)
        {
            Console.WriteLine($"ID : {user.Id}, Nom : {user.Name}, Email : {user.Email}");
        }
    }
    
    internal static void TestGetTacticsOfAllUsers(AppContext.AppContext db)
    {
        Console.WriteLine("Récupération des tactiques de tous les utilisateurs :");

        var users = db.Users.Include(u => u.Tactics).ToList();
        foreach (var user in users)
        {
            Console.WriteLine($"Tactiques de l'utilisateur {user.Name}:");
            foreach (var tactic in user.Tactics)
            {
                Console.WriteLine($"\tID : {tactic.Id}, Nom : {tactic.Name}, Date de création : {tactic.CreationDate}");
            }
        }
    }

    internal static void TestGetTacticsOfOneUser(AppContext.AppContext db, int userId)
    {
        var user = db.Users.Include(u => u.Tactics).FirstOrDefault(u => u.Id == userId);
        if (user != null)
        {
            Console.WriteLine($"Récupération des tactiques de l'utilisateur {user.Name}:");
            foreach (var tactic in user.Tactics)
            {
                Console.WriteLine($"\tID : {tactic.Id}, Nom : {tactic.Name}, Date de création : {tactic.CreationDate}");
            }
        }
        else
        {
            Console.WriteLine($"Aucun utilisateur trouvé avec l'ID : {userId}");
        }
    }

}
