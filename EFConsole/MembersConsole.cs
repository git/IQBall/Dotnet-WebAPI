﻿using System;
using System.Linq;
using AppContext.Entities;
using Model;

namespace EFConsole
{
    public class MembersConsole
    {
        public static void TestAddMember(AppContext.AppContext db)
        {
            var newMember = new MemberEntity
            {
                TeamId = 1,
                UserId = 1,
                Role = MemberRole.Player
            };

            db.Members.Add(newMember);
            db.SaveChanges();

            Console.WriteLine("Membre ajouté avec succès !");
        }

        public static void TestGetAllMembers(AppContext.AppContext db)
        {
            var members = db.Members.ToList();
            Console.WriteLine("Liste des membres :");
            foreach (var member in members)
            {
                Console.WriteLine($"ID Équipe : {member.TeamId}, ID Utilisateur : {member.UserId}, Rôle : {member.Role}");
            }
        }

        public static void TestGetMembersByTeamId(AppContext.AppContext db, int teamId)
        {
            var members = db.Members.Where(m => m.TeamId == teamId).ToList();
            Console.WriteLine($"Membres de l'équipe avec ID {teamId} :");
            foreach (var member in members)
            {
                Console.WriteLine($"ID Équipe : {member.TeamId}, ID Utilisateur : {member.UserId}, Rôle : {member.Role}");
            }
        }

        public static void TestGetMembersByUserId(AppContext.AppContext db, int userId)
        {
            var members = db.Members.Where(m => m.UserId == userId).ToList();
            Console.WriteLine($"Membres associés à l'utilisateur avec ID {userId} :");
            foreach (var member in members)
            {
                Console.WriteLine($"ID Équipe : {member.TeamId}, ID Utilisateur : {member.UserId}, Rôle : {member.Role}");
            }
        }

        public static void TestUpdateMemberRole(AppContext.AppContext db, int teamId, int memberId, MemberRole newRole)
        {
            var memberToUpdate = db.Members.FirstOrDefault(m => m.TeamId == teamId && m.UserId == memberId); // Trouver le membre dans l'équipe spécifiée
            if (memberToUpdate != null)
            {
                memberToUpdate.Role = newRole;
                db.SaveChanges();
                Console.WriteLine("Rôle du membre mis à jour avec succès !");
            }
            else
            {
                Console.WriteLine($"Aucun membre trouvé dans l'équipe avec l'ID {teamId} et l'ID utilisateur {memberId}.");
            }
        }


        public static void TestRemoveMember(AppContext.AppContext db, int teamId, int memberId)
        {
            var memberToDelete = db.Members.FirstOrDefault(m => m.TeamId == teamId && m.UserId == memberId); // Trouver le membre dans l'équipe spécifiée
            if (memberToDelete != null)
            {
                db.Members.Remove(memberToDelete); // Supprimer le membre
                db.SaveChanges();
                Console.WriteLine("Membre supprimé avec succès !");
            }
            else
            {
                Console.WriteLine($"Aucun membre trouvé dans l'équipe avec l'ID {teamId} et l'ID utilisateur {memberId} pour la suppression.");
            }
        }


    }
}
