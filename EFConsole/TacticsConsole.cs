﻿using System;
using System.Linq;
using AppContext.Entities;
using Model;

namespace EFConsole
{
    class TacticsConsole
    {
        internal static void TestAddTactic(AppContext.AppContext db)
        {
            var newTactic = new TacticEntity
            {
                Name = "Nouvelle tactique",
                CreationDate = DateTime.Now,
                OwnerId = 1,
                Type = CourtType.Plain
            };

            db.Tactics.Add(newTactic);
            db.SaveChanges();

            Console.WriteLine("Tactique ajoutée avec succès !");
        }

        internal static void TestGetAllTactics(AppContext.AppContext db)
        {
            var tactics = db.Tactics.ToList();
            Console.WriteLine("Liste des tactiques :");
            foreach (var tactic in tactics)
            {
                Console.WriteLine($"ID : {tactic.Id}, Nom : {tactic.Name}, Date de création : {tactic.CreationDate}");
            }
        }

        internal static void TestFindTacticById(AppContext.AppContext db, int tacticId)
        {
            var tactic = db.Tactics.FirstOrDefault(t => t.Id == tacticId);
            if (tactic != null)
            {
                Console.WriteLine($"Tactique trouvée avec l'ID {tacticId}: Nom : {tactic.Name}, Date de création : {tactic.CreationDate}");
            }
            else
            {
                Console.WriteLine($"Aucune tactique trouvée avec l'ID {tacticId}");
            }
        }

        internal static void TestUpdateTactic(AppContext.AppContext db, int tacticId, string newName)
        {
            var tacticToUpdate = db.Tactics.FirstOrDefault(t => t.Id == tacticId);
            if (tacticToUpdate != null)
            {
                tacticToUpdate.Name = newName;
                db.SaveChanges();
                Console.WriteLine($"Tactique mise à jour avec succès !");
            }
            else
            {
                Console.WriteLine($"Aucune tactique trouvée avec l'ID {tacticId}");
            }
        }

        internal static void TestDeleteTactic(AppContext.AppContext db, int tacticId)
        {
            var tacticToDelete = db.Tactics.FirstOrDefault(t => t.Id == tacticId);
            if (tacticToDelete != null)
            {
                db.Tactics.Remove(tacticToDelete);
                db.SaveChanges();
                Console.WriteLine($"Tactique supprimée avec succès !");
            }
            else
            {
                Console.WriteLine($"Aucune tactique trouvée avec l'ID {tacticId}");
            }
        }
        
        internal static void TestGetTacticsByOwner(AppContext.AppContext db, int ownerId)
        {
            var tactics = db.Tactics.Where(t => t.OwnerId == ownerId).ToList();
            Console.WriteLine($"Tactiques de l'utilisateur avec l'ID {ownerId} :");
            foreach (var tactic in tactics)
            {
                Console.WriteLine($"ID : {tactic.Id}, Nom : {tactic.Name}, Date de création : {tactic.CreationDate}");
            }
        }
    }
}
