﻿using System;
using System.Linq;
using AppContext.Entities;

namespace EFConsole
{
    class TeamsConsole
    {
        internal static void TestAddTeam(AppContext.AppContext db)
        {
            var newTeam = new TeamEntity
            {
                Name = "Nouvelle équipe",
                Picture = "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png",
                MainColor = "#0000FF",
                SecondColor = "#FFFFFF"
            };

            db.Teams.Add(newTeam);
            db.SaveChanges();

            Console.WriteLine("Équipe ajoutée avec succès !");
        }

        internal static void TestGetAllTeams(AppContext.AppContext db)
        {
            var teams = db.Teams.ToList();
            Console.WriteLine("Liste des équipes :");
            foreach (var team in teams)
            {
                Console.WriteLine($"ID : {team.Id}, Nom : {team.Name}, Image : {team.Picture}, Couleur principale : {team.MainColor}, Couleur secondaire : {team.SecondColor}");
            }
        }

        internal static void TestGetTeamMembers(AppContext.AppContext db, int teamId)
        {
            var team = db.Teams.FirstOrDefault(t => t.Id == teamId);
            if (team != null)
            {
                Console.WriteLine($"Membres de l'équipe '{team.Name}' :");
                foreach (var member in team.Members)
                {
                    Console.WriteLine($"ID : {member.UserId}, Nom : {member.User?.Name}, Rôle : {member.Role}");
                }
            }
            else
            {
                Console.WriteLine($"Aucune équipe trouvée avec l'ID : {teamId}");
            }
        }

        internal static void TestUpdateTeam(AppContext.AppContext db, int teamId, string newName, string newPicture, string newMainColor, string newSecondColor)
        {
            var teamToUpdate = db.Teams.FirstOrDefault(t => t.Id == teamId);
            if (teamToUpdate != null)
            {
                teamToUpdate.Name = newName;
                teamToUpdate.Picture = newPicture;
                teamToUpdate.MainColor = newMainColor;
                teamToUpdate.SecondColor = newSecondColor;
                db.SaveChanges();
                Console.WriteLine("Équipe mise à jour avec succès !");
            }
            else
            {
                Console.WriteLine($"Aucune équipe trouvée avec l'ID : {teamId}");
            }
        }

        internal static void TestDeleteTeam(AppContext.AppContext db, int teamId)
        {
            var teamToDelete = db.Teams.FirstOrDefault(t => t.Id == teamId);
            if (teamToDelete != null)
            {
                db.Teams.Remove(teamToDelete);
                db.SaveChanges();
                Console.WriteLine("Équipe supprimée avec succès !");
            }
            else
            {
                Console.WriteLine($"Aucune équipe trouvée avec l'ID : {teamId}");
            }
        }
    }
}
