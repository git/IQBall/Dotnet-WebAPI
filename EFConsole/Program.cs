﻿using Model;
using StubContext;

namespace EFConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            try
            {
                using (AppContext.AppContext db = new StubAppContext())
                {
                    TestUserMethods(db);
                    TestTacticMethods(db);
                    TestTeamMethods(db);
                    TestMemberMethods(db);
                    TestTacticsStepMethods(db);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Une erreur s'est produite : {ex.Message}");

                if (ex.InnerException != null)
                {
                    Console.WriteLine($"Détails de l'exception interne : {ex.InnerException.Message}");
                }
            }
        }

        static void TestUserMethods(AppContext.AppContext db)
        {
            UsersConsole.TestAddUser(db);
            UsersConsole.TestGetAllUsers(db);
            UsersConsole.TestFindUserByMail(db, "maxime@mail.com");
            UsersConsole.TestUpdateUser(db);
            UsersConsole.TestDeleteUser(db);
            UsersConsole.TestSearchUsersByName(db, "Pierre");
            UsersConsole.TestGetTacticsOfAllUsers(db);
            UsersConsole.TestGetTacticsOfOneUser(db, 1);
        }

        static void TestTacticMethods(AppContext.AppContext db)
        {
            TacticsConsole.TestAddTactic(db);
            TacticsConsole.TestGetAllTactics(db);
            TacticsConsole.TestFindTacticById(db, 1);
            TacticsConsole.TestUpdateTactic(db, 1, "Nouveau nom");
            TacticsConsole.TestDeleteTactic(db, 1);
            TacticsConsole.TestGetTacticsByOwner(db, 1);
        }
        
        static void TestTeamMethods(AppContext.AppContext db)
        {
            TeamsConsole.TestAddTeam(db);
            TeamsConsole.TestGetAllTeams(db);
            TeamsConsole.TestGetTeamMembers(db, 1);
            TeamsConsole.TestUpdateTeam(db, 1, "Nouveau nom", "Nouvelle image", "#FF0000", "#00FF00");
            TeamsConsole.TestDeleteTeam(db, 1);
        }
        
        static void TestMemberMethods(AppContext.AppContext db)
        {
            MembersConsole.TestAddMember(db);
            MembersConsole.TestGetAllMembers(db);
            MembersConsole.TestGetMembersByTeamId(db, 1);
            MembersConsole.TestGetMembersByUserId(db, 1);
            MembersConsole.TestUpdateMemberRole(db, 1, 1, MemberRole.Coach);
            MembersConsole.TestRemoveMember(db, 1, 1);
        }
        
        static void TestTacticsStepMethods(AppContext.AppContext db)
        {
            TacticsStepConsole.TestAddTacticStep(db);
            TacticsStepConsole.TestGetAllTacticSteps(db);
            TacticsStepConsole.TestGetTacticStepsByTacticId(db, 1);
            TacticsStepConsole.TestUpdateTacticStepContent(db, 1, "test content");
            TacticsStepConsole.TestDeleteTacticStep(db, 1);
        }

    }
}