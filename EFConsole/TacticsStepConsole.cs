﻿using System;
using System.Linq;
using AppContext.Entities;
using Model;

namespace EFConsole
{
    public class TacticsStepConsole
    {
        public static void TestAddTacticStep(AppContext.AppContext db)
        {
            var newTacticStep = new TacticStepEntity
            {
                ParentId = 1,
                TacticId = 1,
                JsonContent = "{}"
            };

            db.TacticSteps.Add(newTacticStep);
            db.SaveChanges();

            Console.WriteLine("Étape de tactique ajoutée avec succès !");
        }

        public static void TestGetAllTacticSteps(AppContext.AppContext db)
        {
            var tacticSteps = db.TacticSteps.ToList();
            Console.WriteLine("Liste des étapes de tactique :");
            foreach (var tacticStep in tacticSteps)
            {
                Console.WriteLine($"ID : {tacticStep.Id}, ID Tactique : {tacticStep.TacticId}, Contenu JSON : {tacticStep.JsonContent}");
            }
        }

        public static void TestGetTacticStepsByTacticId(AppContext.AppContext db, int tacticId)
        {
            var tacticSteps = db.TacticSteps.Where(ts => ts.TacticId == tacticId).ToList();
            Console.WriteLine($"Étapes de tactique pour la tactique avec l'ID {tacticId} :");
            foreach (var tacticStep in tacticSteps)
            {
                Console.WriteLine($"ID : {tacticStep.Id}, ID Tactique : {tacticStep.TacticId}, Contenu JSON : {tacticStep.JsonContent}");
            }
        }

        public static void TestUpdateTacticStepContent(AppContext.AppContext db, int tacticStepId, string newContent)
        {
            var tacticStepToUpdate = db.TacticSteps.FirstOrDefault(ts => ts.Id == tacticStepId);
            if (tacticStepToUpdate != null)
            {
                tacticStepToUpdate.JsonContent = newContent;
                db.SaveChanges();
                Console.WriteLine("Contenu de l'étape de tactique mis à jour avec succès !");
            }
            else
            {
                Console.WriteLine($"Aucune étape de tactique trouvée avec l'ID {tacticStepId} pour la mise à jour du contenu.");
            }
        }

        public static void TestDeleteTacticStep(AppContext.AppContext db, int tacticStepId)
        {
            var tacticStepToDelete = db.TacticSteps.FirstOrDefault(ts => ts.Id == tacticStepId);
            if (tacticStepToDelete != null)
            {
                db.TacticSteps.Remove(tacticStepToDelete);
                db.SaveChanges();
                Console.WriteLine("Étape de tactique supprimée avec succès !");
            }
            else
            {
                Console.WriteLine($"Aucune étape de tactique trouvée avec l'ID {tacticStepId} pour la suppression.");
            }
        }
    }
}
