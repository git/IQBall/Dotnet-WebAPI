using Services.Failures;

namespace Services;

public class ServiceException : Exception
{
    public List<Failure> Failures { get; init; }

    public ServiceException(params Failure[] failures)
    {
        Failures = new List<Failure>(failures);
    }

    public Dictionary<string, string[]> FailuresMessages()
    {
        return Failures.GroupBy(f => f.Name)
            .Select(f => (f.Key, f.Select(f => f.Message).ToArray()))
            .ToDictionary();
    }
}