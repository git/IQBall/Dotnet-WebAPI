namespace Services.Failures;

public record Failure(string Name, string Message)
{
    public static Failure NotFound(string message)
    {
        return new("not found", message);
    }
    

}