using Model;

namespace Services;

/// <summary>
/// Represents a service interface for managing tactics.
/// </summary>
public interface ITacticService
{
    /// <summary>
    /// Retrieves a list of tactics owned by the specified user.
    /// </summary>
    /// <param name="userId">The ID of the user.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains a list of tactics.</returns>
    Task<IEnumerable<Tactic>> ListTacticsOf(int userId);

    /// <summary>
    /// Checks if the userId corresponds to the tactic's owner identifier
    /// </summary>
    /// <param name="userId">The ID of the user.</param>
    /// <param name="tacticId">The ID of the tactic.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains a boolean indicating whether the user has rights.</returns>
    Task<bool> IsOwnerOf(int userId, int tacticId);

    /// <summary>
    /// Adds a new tactic for the specified user.
    /// </summary>
    /// <param name="userId">The ID of the user.</param>
    /// <param name="name">The name of the tactic.</param>
    /// <param name="courtType">The type of court.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains the ID of the newly added tactic.</returns>
    Task<(int, int)> AddTactic(int userId, string name, CourtType courtType);

    /// <summary>
    /// Updates the name of the specified tactic.
    /// </summary>
    /// <param name="tacticId">The ID of the tactic.</param>
    /// <param name="name">The new name of the tactic.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains a boolean indicating whether the update was successful.</returns>
    Task<bool> UpdateName(int tacticId, string name);

    /// <summary>
    /// Sets the content of a tactic step.
    /// </summary>
    /// <param name="tacticId">The ID of the tactic.</param>
    /// <param name="stepId">The ID of the step.</param>
    /// <param name="json">The JSON content to set.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains a boolean indicating whether the operation was successful.</returns>
    Task<bool> SetTacticStepContent(int tacticId, int stepId, string json);

    /// <summary>
    /// Retrieves the content of a tactic step.
    /// </summary>
    /// <param name="tacticId">The ID of the tactic.</param>
    /// <param name="stepId">The ID of the step.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains the JSON content of the step.</returns>
    Task<string?> GetTacticStepContent(int tacticId, int stepId);


    public Task<bool> ShareTactic(int tacticId, int? userId, int? teamId);
    public Task<bool> UnshareTactic(int tacticId, int? userId, int? teamId);

    /// <summary>
    /// Retrieves the root step of the specified tactic.
    /// </summary>
    /// <param name="tacticId">The ID of the tactic.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains the root step of the tactic.</returns>
    Task<TacticStep> GetRootStep(int tacticId);

    /// <summary>
    /// Retrieves the tactic with the specified ID.
    /// </summary>
    /// <param name="tacticId">The ID of the tactic.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains the tactic.</returns>
    Task<Tactic?> GetTactic(int tacticId);

    /// <summary>
    /// Retrieves a list of tactics owned by the specified user.
    /// </summary>
    /// <param name="userId">The ID of the user.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains a list of tactics.</returns>
    Task<IEnumerable<Tactic>> ListUserTactics(int userId);

    /// <summary>
    /// Adds a new step to the specified tactic.
    /// </summary>
    /// <param name="tacticId">The ID of the tactic.</param>
    /// <param name="parentStepId">The ID of the parent step.</param>
    /// <param name="initialJson">The initial JSON content of the step.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains the ID of the newly added step.</returns>
    Task<int?> AddTacticStep(int tacticId, int parentStepId, string initialJson);

    /// <summary>
    /// Removes the specified step from the tactic, along with its child steps if any.
    /// </summary>
    /// <param name="tacticId">The ID of the tactic.</param>
    /// <param name="stepId">The ID of the step to remove.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains a boolean indicating whether the removal was successful.</returns>
    Task<bool> RemoveTacticStep(int tacticId, int stepId);

    Task<bool> RemoveTactic(int tacticId);
}