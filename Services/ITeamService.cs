using Model;

namespace Services;

/// <summary>
/// Represents a service for managing teams.
/// </summary>
public interface ITeamService
{
    /// <summary>
    /// Lists all teams associated with the specified <paramref name="userId"/>.
    /// </summary>
    Task<IEnumerable<Team>> ListTeamsOf(int userId);

    /// <summary>
    /// Lists a range of teams.
    /// </summary>
    Task<IEnumerable<Team>> ListTeams(int start, int count);

    /// <summary>
    /// Retrieves the total count of teams.
    /// </summary>
    Task<int> CountTotalTeams();

    /// <summary>
    /// Adds a new team.
    /// </summary>
    Task<Team> AddTeam(string name, string picture, string firstColor, string secondColor);
    
    Task<Team?> GetTeam(int id);

    /// <summary>
    /// Removes one or more teams.
    /// </summary>
    Task RemoveTeams(params int[] teams);

    /// <summary>
    /// Updates an existing team.
    /// </summary>
    Task<bool> UpdateTeam(Team team);

    /// <summary>
    /// Retrieves the members of the specified team.
    /// </summary>
    Task<IEnumerable<Member>> GetMembersOf(int teamId);

    /// <summary>
    /// Adds a new member to the team.
    /// </summary>
    Task<Member?> AddMember(int teamId, int userId, MemberRole role);

    /// <summary>
    /// Updates the role of a member within the team.
    /// </summary>
    Task<bool> UpdateMember(Member member);

    /// <summary>
    /// Removes a member from the team.
    /// </summary>
    Task<bool> RemoveMember(int teamId, int userId);


    enum TeamAccessibility
    {
        /**
         * The Team or the user is not found
         */
        NotFound,

        /**
         * Accessibility not granted
         */
        Unauthorized,

        /**
         * Accessibility granted
         */
        Authorized
    }
 
    public Task<IEnumerable<Tactic>> GetSharedTacticsToTeam(int teamId);

    public Task<bool> IsUserInTeam(int userId, int teamId);


    /**
     * Ensures that the given user identifier van perform an operation that requires the given role permission.
     * The returned result is the different kind of accessibility the service can grant to the user, based on its actual role inside the
     * given team.
     */
    public Task<TeamAccessibility> EnsureAccessibility(int userId, int teamId, MemberRole role);

}