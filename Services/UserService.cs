﻿using Model;

namespace Services;

/// <summary>
/// Represents a service for managing users.
/// </summary>
public interface IUserService
{
    /// <summary>
    /// Retrieves the count of users whose names contain the specified needle.
    /// </summary>
    Task<int> UsersCount(string nameNeedle);

    /// <summary>
    /// Retrieves the total count of users.
    /// </summary>
    Task<int> UsersCount();

    /// <summary>
    /// Lists a range of users, optionally filtering by name.
    /// </summary>
    Task<IEnumerable<User>> ListUsers(int start, int count, string? nameNeedle = null);

    /// <summary>
    /// Retrieves the user with the specified ID.
    /// </summary>
    Task<User?> GetUser(int id);

    /// <summary>
    /// Retrieves the user with the specified email.
    /// </summary>
    Task<User?> GetUser(string email);

    /// <summary>
    /// Creates a new user.
    /// </summary>
    Task<User> CreateUser(string username, string email, string password, string profilePicture, bool isAdmin);

    /// <summary>
    /// Removes one or more users.
    /// </summary>
    Task<bool> RemoveUsers(params int[] identifiers);

    /// <summary>
    /// Updates an existing user.
    /// </summary>
    Task UpdateUser(User user, string? password = null);
    
    
    public Task<IEnumerable<Tactic>> GetSharedTacticsToUser(int userId);
    

    /// <summary>
    /// Authorizes a user with the specified email and password.
    /// </summary>
    Task<User?> Authorize(string email, string password);
}