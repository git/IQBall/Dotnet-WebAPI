using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace DbServices;

public class Hashing
{
    public static (string, byte[]) HashString(string str)
    {
        byte[] salt = RandomNumberGenerator.GetBytes(128 / 8);
        string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: str,
            salt,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 50000,
            numBytesRequested: 256 / 8
        ));

        return (hashed, salt);
    }

    public static bool PasswordsMatches(string password, string str, byte[] salt)
    {
        string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: str,
            salt,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 50000,
            numBytesRequested: 256 / 8
        ));

        return hashed == password;
    }

}