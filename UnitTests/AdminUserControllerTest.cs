using API.Controllers;
using API.Controllers.Admin;
using DbServices;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Model;
using StubContext;


namespace UnitTests;

public class AdminUserControllerTest
{
    private static UsersAdminController GetUsersController()
    {
        var connection = new SqliteConnection("Data Source=:memory:");
        connection.Open();
        var context = new StubAppContext(
            new DbContextOptionsBuilder<AppContext.AppContext>()
                .UseSqlite(connection)
                .Options
        );
        context.Database.EnsureCreated();
        var service = new DbUserService(context);
        return new UsersAdminController(service, new LoggerFactory().CreateLogger<UsersAdminController>());
    }


    [Fact]
    public async void CountUsersTest()
    {
        var controller = GetUsersController();
        (await controller.CountUsers()).Should().Be(new UsersAdminController.CountUsersResponse(5));
        (await controller.CountUsers("a")).Should().BeEquivalentTo(new UsersAdminController.CountUsersResponse(3));
        (await controller.CountUsers("")).Should().BeEquivalentTo(new UsersAdminController.CountUsersResponse(5));
        (await controller.CountUsers("^ù$*")).Should().BeEquivalentTo(new UsersAdminController.CountUsersResponse(0));
    }

    [Fact]
    public async void ListUsersTest()
    {
        var controller = GetUsersController();
        (await controller.CountUsers()).Should().Be(new UsersAdminController.CountUsersResponse(5));
        (await controller.ListUsers(0, 10, null)).Should().BeEquivalentTo(new List<User>
        {
            new(1, "maxime", "maxime@mail.com",
                UsersController.DefaultProfilePicture, true),
            new(2, "mael", "mael@mail.com",
                UsersController.DefaultProfilePicture, true),
            new(3, "yanis", "yanis@mail.com",
                UsersController.DefaultProfilePicture, true),
            new(4, "simon", "simon@mail.com",
                UsersController.DefaultProfilePicture, true),
            new(5, "vivien", "vivien@mail.com",
                UsersController.DefaultProfilePicture, true),
        });
        (await controller.ListUsers(0, 10, "")).Should().BeEquivalentTo(new List<User>
        {
            new(1, "maxime", "maxime@mail.com",
                UsersController.DefaultProfilePicture, true),
            new(2, "mael", "mael@mail.com",
                UsersController.DefaultProfilePicture, true),
            new(3, "yanis", "yanis@mail.com",
                UsersController.DefaultProfilePicture, true),
            new(4, "simon", "simon@mail.com",
                UsersController.DefaultProfilePicture, true),
            new(5, "vivien", "vivien@mail.com",
                UsersController.DefaultProfilePicture, true),
        });
        (await controller.ListUsers(0, 10, "a")).Should().BeEquivalentTo(new List<User>
        {
            new(1, "maxime", "maxime@mail.com",
                UsersController.DefaultProfilePicture, true),
            new(2, "mael", "mael@mail.com",
                UsersController.DefaultProfilePicture, true),
            new(3, "yanis", "yanis@mail.com",
                UsersController.DefaultProfilePicture, true),
        });
        (await controller.ListUsers(0, 0, "")).Should().BeEquivalentTo(new List<User> { });
        (await controller.ListUsers(0, 10, "^ù$*")).Should().BeEquivalentTo(new List<User> { });
    }

    [Fact]
    public async void GetUserTest()
    {
        var controller = GetUsersController();
        var response = await controller.GetUser(0);
        response.Should().BeEquivalentTo(controller.NotFound());

        response = await controller.GetUser(1);
        response.Should().BeEquivalentTo(controller.Ok(new User(1, "maxime", "maxime@mail.com",
            UsersController.DefaultProfilePicture, true)));
    }

    [Fact]
    public async void AddUserTest()
    {
        var controller = GetUsersController();
        var user = await controller.AddUser(new("Test", "TestPassword", "test@mail.com", true));
        user.Should().BeEquivalentTo(new User(6, "Test", "test@mail.com", UsersController.DefaultProfilePicture, true));
    }

    [Fact]
    public async void RemoveUsersTest()
    {
        var controller = GetUsersController();
        var result = await controller.RemoveUsers(new([1, 4, 3, 5]));
        result.Should().BeEquivalentTo(controller.Ok());

        var remainingUsers = await controller.ListUsers(0, 10, null);
        remainingUsers.Should().BeEquivalentTo(new User[] { new(2, "mael", "mael@mail.com",
            UsersController.DefaultProfilePicture, true) });
    }

    [Fact]
    public async void UpdateUserTest()
    {
        var controller = GetUsersController();
        var result = await controller.UpdateUser(1, new("maxou", "maxou@mail.com", false));
        result.Should().BeEquivalentTo(controller.Ok());

        var userResult = await controller.GetUser(1);
        userResult.Should().BeEquivalentTo(controller.Ok(new User(1, "maxou", "maxou@mail.com", UsersController.DefaultProfilePicture, false)));
        
        
        result = await controller.UpdateUser(10, new("maxou", "maxou@mail.com", false));
        result.Should().BeEquivalentTo(controller.BadRequest());
    }
}