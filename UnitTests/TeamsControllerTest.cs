using API.Controllers;
using DbServices;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using StubContext;

namespace UnitTests;

public class TeamsControllerTest
{
    private static (TeamsController, AppContext.AppContext) GetController(int userId)
    {
        var connection = new SqliteConnection("Data Source=:memory:");
        connection.Open();
        var context = new StubAppContext(
            new DbContextOptionsBuilder<AppContext.AppContext>()
                .UseSqlite(connection)
                .Options
        );
        context.Database.EnsureCreated();
        var controller = new TeamsController(
            new DbTeamService(context), 
            new DbTacticService(context),
            new DbUserService(context),
            new ManualContextAccessor(userId)
        );

        return (controller, context);
    }

    [Fact]
    public async void CreateTeamTest()
    {
        var (controller, context) = GetController(1);
        var result = await controller.CreateTeam(new("Space Station", "55652433-1DB1-4778-A1FA-D56060BA1F1C", "#FFFFFF",
            "#AAFFBB"));
        result.Should().BeEquivalentTo(controller.Ok(new Team(3, "Space Station",
            "55652433-1DB1-4778-A1FA-D56060BA1F1C", "#FFFFFF", "#AAFFBB")));
        (await context.Teams.FindAsync(3))!.Name.Should().BeEquivalentTo("Space Station");
        (await context.Members.AnyAsync(m => m.TeamId == 3 && m.UserId == 1)).Should().BeTrue();
    }

    [Fact]
    public async void GetMembersOfTest()
    {
        var (controller, context) = GetController(1);
        var result = await controller.GetMembersOf(1);
        // result.Should().BeEquivalentTo(controller.Ok(new Member[]
        // {
        //     new(1, 1, MemberRole.Coach),
        //     new(1, 2, MemberRole.Player)
        // }));
    }

    [Fact]
    public async void AddMemberTest()
    {
        var (controller, context) = GetController(1);
        var result = await controller.AddMember(1, new(3, "COACH"));
        result.Should().BeEquivalentTo(controller.Ok(new Member(1, 3, MemberRole.Coach)));

        (await context.Members.AnyAsync(m => m.TeamId == 1 && m.UserId == 3)).Should().BeTrue();

        result = await controller.AddMember(1, new(3, "COACH"));
        result.Should().BeEquivalentTo(controller.Forbid());
    }

    [Fact]
    public async void UpdateMemberTest()
    {
        var (controller, context) = GetController(1);
        var result = await controller.UpdateMember(1, 2, new("COACH"));
        result.Should().BeEquivalentTo(controller.Ok());
        
        (await context.Members.FirstAsync(m => m.TeamId == 1 && m.UserId == 2)).Role.Should().Be(MemberRole.Coach);
        
        result = await controller.UpdateMember(10, 2, new("COACH"));
        result.Should().BeEquivalentTo(controller.NotFound());
        
        result = await controller.UpdateMember(1, 3, new("COACH"));
        result.Should().BeEquivalentTo(controller.NotFound());
    }
    
    
    [Fact]
    public async void RemoveMemberTest()
    {
        var (controller, context) = GetController(1);
        var result = await controller.RemoveMember(1, 2);
        result.Should().BeEquivalentTo(controller.Ok());
        
        result = await controller.RemoveMember(10, 2);
        result.Should().BeEquivalentTo(controller.NotFound());
        
        result = await controller.RemoveMember(1, 3);
        result.Should().BeEquivalentTo(controller.NotFound());
    }
}