using API.Controllers;
using API.DTO;
using DbServices;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using StubContext;

namespace UnitTests;

public class TacticsControllerTest
{
    private static (TacticController, AppContext.AppContext) GetController(int userId)
    {
        var connection = new SqliteConnection("Data Source=:memory:");
        connection.Open();
        var context = new StubAppContext(
            new DbContextOptionsBuilder<AppContext.AppContext>()
                .UseSqlite(connection)
                .Options
        );
        context.Database.EnsureCreated();
        var controller = new TacticController(
            new DbTacticService(context),
            new ManualContextAccessor(userId)
        );

        return (controller, context);
    }

    [Fact]
    public async void UpdateName()
    {
        var (controller, context) = GetController(1);
        var result = await controller.UpdateName(1, new("Stade de France"));
        result.Should().BeEquivalentTo(controller.Ok());

        var tactic = await context.Tactics.FindAsync(1);
        tactic.Name.Should().BeEquivalentTo("Stade de France");

        result = await controller.UpdateName(-1, new("Stade de France"));
        result.Should().BeEquivalentTo(controller.Unauthorized());
    }

    [Fact]
    public async void GetTacticInfoTest()
    {
        var (controller, context) = GetController(1);
        var result = await controller.GetTacticInfo(1);
        result.Should()
            .BeEquivalentTo(controller.Ok(new Tactic(1, "New tactic", 1, CourtType.Plain, new DateTime(2024, 5, 31))
                .ToDto()));

        result = await controller.GetTacticInfo(100);
        result.Should().BeEquivalentTo(controller.Unauthorized());
    }

    [Fact]
    public async void GetTacticStepsRootTest()
    {
        var (controller, context) = GetController(1);
        var result = await controller.GetTacticStepsRoot(1);
        result.Should()
            .BeEquivalentTo(controller.Ok(
                new TacticController.GetTacticStepsTreeResponse(new TacticStep(1, null, [], "{}").ToDto())));
    }

    [Fact]
    public async void CreateNewTest()
    {
        var (controller, context) = GetController(1);
        var result = await controller.CreateNew(new("Test Tactic", "pLaIn"));
        result.Should().BeEquivalentTo(new TacticController.CreateNewResponse(2, 2));
        var tactic = await context.Tactics.FirstOrDefaultAsync(e => e.Id == 2);
        tactic.Should().NotBeNull();
        tactic!.Name.Should().BeEquivalentTo("Test Tactic");
        tactic.OwnerId.Should().Be(1);
        tactic.Type.Should().Be(CourtType.Plain);
    }


    [Fact]
    public async void AddStepTest()
    {
        var (controller, context) = GetController(1);
        var result = await controller.AddStep(1, new(1, "{components: []}"));
        result.Should().BeEquivalentTo(controller.Ok(new TacticController.AddStepResponse(2)));
        var tactic = await context.TacticSteps.FirstOrDefaultAsync(e => e.Id == 2);
        tactic.Should().NotBeNull();
        tactic!.Id.Should().Be(2);
        tactic.ParentId.Should().Be(1);
        tactic.TacticId.Should().Be(1);

        // if tactic does not exists
        result = await controller.AddStep(100, new(1, "hello"));
        result.Should().BeEquivalentTo(controller.NotFound());

        // if step does not exists
        result = await controller.AddStep(1, new(10, "hello"));
        result.Should().BeEquivalentTo(controller.NotFound());
    }

    [Fact]
    public async void RemoveStepTest()
    {
        var (controller, context) = GetController(1);
        var result = await controller.RemoveStep(1, 1);
        result.Should().BeEquivalentTo(controller.Ok());
        var tactic = await context.TacticSteps.FirstOrDefaultAsync(e => e.Id == 1);
        tactic.Should().BeNull();

        // if tactic does not exists
        result = await controller.RemoveStep(100, 1);
        result.Should().BeEquivalentTo(controller.Unauthorized());

        // if step does not exists
        result = await controller.RemoveStep(1, 10);
        result.Should().BeEquivalentTo(controller.NotFound());
    }

    [Fact]
    public async void GetStepContentTest()
    {
        var (controller, context) = GetController(1);
        (await controller.GetStepContent(1, 1)).Should().BeAssignableTo(controller.Ok("").GetType());
        (await controller.GetStepContent(10, 1)).Should().BeEquivalentTo(controller.Unauthorized());
        (await controller.GetStepContent(1, 10)).Should().BeEquivalentTo(controller.NotFound());
    }

    [Fact]
    public async void SaveStepContentTest()
    {
        var (controller, context) = GetController(1);
        (await controller.SaveStepContent(1, 1, new(new object()))).Should().BeEquivalentTo(controller.Ok());
        (await controller.SaveStepContent(10, 1, new (new object()))).Should().BeEquivalentTo(controller.Unauthorized());
        (await controller.SaveStepContent(1, 10, new (new object()))).Should().BeEquivalentTo(controller.NotFound());
    }
}