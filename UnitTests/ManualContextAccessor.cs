using API.Context;
using Microsoft.AspNetCore.Http;

namespace UnitTests;

public class ManualContextAccessor(int userId) : IContextAccessor
{
    public int CurrentUserId(HttpContext ctx)
    {
        return userId;
    }
}