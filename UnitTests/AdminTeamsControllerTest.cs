using API.Controllers.Admin;
using DbServices;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Model;
using StubContext;

namespace UnitTests;

public class AdminTeamsControllerTest
{
    private static (TeamsAdminController, AppContext.AppContext) GetController()
    {
        var connection = new SqliteConnection("Data Source=:memory:");
        connection.Open();
        var context = new StubAppContext(
            new DbContextOptionsBuilder<AppContext.AppContext>()
                .UseSqlite(connection)
                .Options
        );
        context.Database.EnsureCreated();
        var controller = new TeamsAdminController(
            new DbTeamService(context),
            new LoggerFactory().CreateLogger<TeamsAdminController>()
        );

        return (controller, context);
    }

    [Fact]
    public async void CountTeamsTest()
    {
        var (controller, context) = GetController();
        (await controller.CountTeams()).Should().BeEquivalentTo(new TeamsAdminController.CountTeamsResponse(2));
    }

    [Fact]
    public async void ListTeamsTest()
    {
        var (controller, context) = GetController();
        (await controller.ListTeams(0, 5)).Should().BeEquivalentTo(new Team[]
        {
            new(
                1,
                "Lakers",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Los_Angeles_Lakers_logo.svg/2560px-Los_Angeles_Lakers_logo.svg.png",
                "#FFFFFF",
                "#000000"
            ),
            new(
                2,
                "Auvergne",
                "https://sancy.iut.uca.fr/~lafourcade/img/photo19.jpg",
                "#FFFFFF",
                "#000000"
            )
        });

        (await controller.ListTeams(3, 5)).Should()
            .BeEquivalentTo(new Team[] { });
    }

    [Fact]
    public async void AddTeamTest()
    {
        var (controller, context) = GetController();
        (await controller.AddTeam(new ("PANPAN", "https://ih1.redbubble.net/image.2005529554.3887/bg,f8f8f8-flat,750x,075,f-pad,750x1000,f8f8f8.jpg", "#FFFFFF", "#000000")))
            .Should()
            .BeEquivalentTo(controller.Ok(new Team(3, "PANPAN", "https://ih1.redbubble.net/image.2005529554.3887/bg,f8f8f8-flat,750x,075,f-pad,750x1000,f8f8f8.jpg", "#FFFFFF", "#000000")));

        var teamEntity = await context.Teams.FirstOrDefaultAsync(t => t.Name == "PANPAN");
        teamEntity.Should().NotBeNull();
        teamEntity!.Id.Should().Be(3);
        teamEntity.Picture.Should().BeEquivalentTo("https://ih1.redbubble.net/image.2005529554.3887/bg,f8f8f8-flat,750x,075,f-pad,750x1000,f8f8f8.jpg");
        teamEntity.MainColor.Should().BeEquivalentTo("#FFFFFF");
        teamEntity.SecondColor.Should().BeEquivalentTo("#000000");
    }
    
    
    [Fact]
    public async void UpdateTeamTest()
    {
        var (controller, context) = GetController();
        (await controller.UpdateTeam(1, new ("PANPAN", "https://ih1.redbubble.net/image.2005529554.3887/bg,f8f8f8-flat,750x,075,f-pad,750x1000,f8f8f8.jpg", "#FFFFFF", "#000000")))
            .Should()
            .BeEquivalentTo(controller.Ok());

        var teamEntity = await context.Teams.FirstOrDefaultAsync(t => t.Name == "PANPAN");
        teamEntity.Should().NotBeNull();
        teamEntity!.Id.Should().Be(1);
        teamEntity.Picture.Should().BeEquivalentTo("https://ih1.redbubble.net/image.2005529554.3887/bg,f8f8f8-flat,750x,075,f-pad,750x1000,f8f8f8.jpg");
        teamEntity.MainColor.Should().BeEquivalentTo("#FFFFFF");
        teamEntity.SecondColor.Should().BeEquivalentTo("#000000");
    }
    
    [Fact]
    public async void DeleteTeamsTest()
    {
        var (controller, context) = GetController();
        (await controller.DeleteTeams(new TeamsAdminController.DeleteTeamsRequest([10, 1, 2])))
            .Should()
            .BeEquivalentTo(controller.Ok());

        (await context.Teams.CountAsync()).Should().Be(0);
        
        (await controller.DeleteTeams(new TeamsAdminController.DeleteTeamsRequest([10, 1, 2])))
            .Should()
            .BeEquivalentTo(controller.Ok());
    }
}