using API.Controllers;
using DbServices;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using StubContext;

namespace UnitTests;

public class AuthControllerTests
{
    public static AuthenticationController GetController()
    {
        var connection = new SqliteConnection("Data Source=:memory:");
        connection.Open();
        var context = new StubAppContext(
            new DbContextOptionsBuilder<AppContext.AppContext>()
                .UseSqlite(connection)
                .Options
        );
        context.Database.EnsureCreated();

        var mock = new Mock<IConfiguration>();
        mock.Setup(c => c["JWT:Key"]).Returns("qzdjnqzdjzdnjzdjqzdjzdqzdnzqdnzqdjnzqd");
        
        
        var controller = new AuthenticationController(
            new DbUserService(context),
            mock.Object
        );

        return controller;
    }


    [Fact]
    public async void GenerateTokenTest()
    {
        var controller = GetController();
        var result = await controller.GenerateToken(new("maxime@mail.com", "123456"));
        result.Should()
            .BeAssignableTo(controller.Ok("").GetType());
    }
    
    [Fact]
    public async void RegisterTest()
    {
        var controller = GetController();
        var result = await controller.RegisterAccount(new("test", "test@mail.com", "123456"));
        result.Should()
            .BeAssignableTo(controller.Ok("").GetType());
    }
    
}