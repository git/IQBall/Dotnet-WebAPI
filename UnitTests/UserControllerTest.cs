using API.Controllers;
using DbServices;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using StubContext;

namespace UnitTests;

public class UsersControllerTest
{
    private static UsersController GetUserController(int userId)
    {
        var connection = new SqliteConnection("Data Source=:memory:");
        connection.Open();
        var context = new StubAppContext(
            new DbContextOptionsBuilder<AppContext.AppContext>()
                .UseSqlite(connection)
                .Options
        );
        context.Database.EnsureCreated();
        var controller = new UsersController(
            new DbUserService(context),
            new DbTeamService(context),
            new DbTacticService(context),
            new ManualContextAccessor(userId)
        );

        return controller;
    }

    [Fact]
    public async void GetCurrentUserTest()
    {
        var controller = GetUserController(1);
        var result = await controller.GetUser();
        result.Should().BeEquivalentTo(new User(1, "maxime", "maxime@mail.com",
            UsersController.DefaultProfilePicture, true));
    }

    [Fact]
    public async void GetUserDataTest()
    {
        var controller = GetUserController(1);
        var result = await controller.GetUserData();
        // result.Should().BeEquivalentTo(new UsersController.GetUserDataResponse(
        //     [new Team(1, "Lakers", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Los_Angeles_Lakers_logo.svg/2560px-Los_Angeles_Lakers_logo.svg.png", "#FFFFFF", "#000000")],
        //     [new TacticDto(1, "New tactic", 1, "PLAIN", 1717106400000L)]
        // ));
    }
    
    [Fact]
    public async Task ShareTacticTest()
    {
        var controller = GetUserController(1);
        var result = await controller.ShareTactic(new UsersController.ShareTacticToUserRequest(1, 2));
        result.Should().BeOfType<OkResult>();
    }
    
    [Fact]
    public async Task GetSharedTacticsToUserTest()
    {
        var controller = GetUserController(2);
        var result = await controller.GetSharedTacticsToUser(2);

        var okResult = result as OkObjectResult;
        var sharedTactics = okResult!.Value as IEnumerable<Tactic>;
        
        sharedTactics!.Should().NotBeNull();
        sharedTactics.Should().ContainSingle();

        var tactic = sharedTactics.First();
        tactic.Id.Should().Be(1);
    }

    [Fact]
    public async Task UnshareTacticTest()
    {
        var controller = GetUserController(1);
        var result = await controller.UnshareTactic(1, 2);
        result.Should().BeOfType<OkResult>();
    }

    [Fact]
    public async Task TestChangeUserInformation()
    {
        var controller = GetUserController(1);
        await controller.ChangeUserInformation(new("a", "b", "c", "d"));
        var user = await controller.GetUser();
        user.Should().BeEquivalentTo(new User(1, "b", "a", "c", true));
    }

}