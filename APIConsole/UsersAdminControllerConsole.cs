﻿using API.Controllers.Admin;
using DbServices;
using Microsoft.Extensions.Logging;
using Services;

namespace APIConsole
{
    public class UsersAdminControllerConsole
    {
        private UsersAdminController _controller;

        public UsersAdminControllerConsole()
        {
            AppContext.AppContext context = new AppContext.AppContext();
            IUserService users = new DbUserService(context);
            ILoggerFactory loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
            ILogger<UsersAdminController> logger = loggerFactory.CreateLogger<UsersAdminController>();
            _controller = new UsersAdminController(users, logger);
        }

        public async void CountUsersTest()
        {
            var result = await _controller.CountUsers();
            Console.WriteLine($"CountUsers Result: {result}");
        }
        
        public async void ListUsersTest()
        {
            var result = await _controller.ListUsers(0, 10, null);
            Console.WriteLine($"ListUsers Result: {result}");
        }
        
        public async void GetUserTest()
        {
            var result = await _controller.GetUser(1);
            Console.WriteLine($"GetUser Result: {result}");
        }

        public async void AddUserTest()
        {
            var result =
                await _controller.AddUser(new UsersAdminController.AddUserRequest("test", "123456", "test@mail.com"));
            Console.WriteLine($"AddUser Result: {result}");
        }
        
        public async void RemoveUserTest()
        {
            var result = await _controller.RemoveUsers(new UsersAdminController.RemoveUsersRequest([1]));
            Console.WriteLine($"RemoveUser Result: {result}");
        }

        public async void UpdateUserTest()
        {
            var result =
                await _controller.UpdateUser(1, new UsersAdminController.UpdateUserRequest("testtest", "123456", false));
            Console.WriteLine($"UpdateUser Result: {result}");
        }

    }
}