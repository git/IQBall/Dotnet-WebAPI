﻿using API.Context;
using API.Controllers;
using DbServices;
using Services;

namespace APIConsole
{
    public class TacticsControllerConsole
    {
        private TacticController _controller;

        public TacticsControllerConsole()
        {
            AppContext.AppContext context = new AppContext.AppContext();
            ITacticService tactics = new DbTacticService(context);
            IContextAccessor accessor = new HttpContextAccessor();
            _controller = new TacticController(tactics, accessor);
        }

        public async void UpdateNameTest()
        {
            var result = await _controller.UpdateName(1, new TacticController.UpdateNameRequest("NewName"));
            Console.WriteLine($"UpdateName Result: {result}");
        }
        
        public async void GetTacticInfoTest()
        {
            var result = await _controller.GetTacticInfo(1);
            Console.WriteLine($"GetTacticInfo Result: {result}");
        }
        
        public async void GetTacticStepsRootTest()
        {
            var result = await _controller.GetTacticStepsRoot(1);
            Console.WriteLine($"GetTacticStepsRoot Result: {result}");
        }
        
        public async void CreateTacticTest()
        {
            var result = await _controller.CreateNew(new TacticController.CreateNewRequest("NewTactic", "PLAIN"));
            Console.WriteLine($"CreateTactic Result: {result}");
        }
        
        public async void GetStepContentTest()
        {
            var result = await _controller.GetStepContent(1, 1);
            Console.WriteLine($"GetStepContent Result: {result}");
        }
        
        public async void RemoveStepTest()
        {
            var result = await _controller.RemoveStep(1, 1);
            Console.WriteLine($"RemoveStep Result: {result}");
        }
        
        public async void SaveStepContentTest()
        {
            var result = await _controller.SaveStepContent(1, 1, new TacticController.SaveStepContentRequest("NewContent"));
            Console.WriteLine($"SaveStepContent Result: {result}");
        }
        

    }
}