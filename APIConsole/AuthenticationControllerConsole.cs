﻿using API.Controllers;
using DbServices;
using Microsoft.AspNetCore.Identity.Data;
using Microsoft.Extensions.Configuration;
using Services;

namespace APIConsole
{
    public class AuthenticationControllerConsole
    {
        private AuthenticationController _controller;

        public AuthenticationControllerConsole()
        {
            AppContext.AppContext context = new AppContext.AppContext();
            IUserService users = new DbUserService(context);
            IConfiguration config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            _controller = new AuthenticationController(users, config);
        }

        public async void RegisterAccountTest()
        {
            var result =
                await _controller.RegisterAccount(
                    new AuthenticationController.RegisterAccountRequest("test", "test@mail.com", "123456"));
            Console.WriteLine($"RegisterAccount Result: {result}");
        }

        public async void GenerateTokenTest()
        {
            var result =
                await _controller.GenerateToken(
                    new AuthenticationController.GenerateTokenRequest("test@mail.com", "123456"));
            Console.WriteLine($"GenerateToken Result: {result}");
        }
    }
}