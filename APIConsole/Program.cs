﻿namespace APIConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var userConsole = new UsersControllerConsole();
            var teamConsole = new TeamsControllerConsole();
            var tacticConsole = new TacticsControllerConsole();
            var authConsole = new AuthenticationControllerConsole();
            var userAdminConsole = new UsersAdminControllerConsole();
            var teamAdminConsole = new TeamsAdminControllerConsole();
            
            userConsole.GetUserTest();
            userConsole.GetUserDataTest();
            
            teamConsole.GetMembersOfTest();
            teamConsole.CreateTeamTest();
            teamConsole.AddMemberTest();
            teamConsole.UpdateMemberTest();
            teamConsole.RemoveMemberTest();
            
            tacticConsole.UpdateNameTest();
            tacticConsole.GetTacticInfoTest();
            tacticConsole.GetTacticStepsRootTest();
            tacticConsole.CreateTacticTest();
            tacticConsole.GetStepContentTest();
            tacticConsole.RemoveStepTest();
            tacticConsole.SaveStepContentTest();
            
            authConsole.RegisterAccountTest();
            authConsole.GenerateTokenTest();
            
            userAdminConsole.CountUsersTest();
            userAdminConsole.ListUsersTest();
            userAdminConsole.GetUserTest();
            userAdminConsole.AddUserTest();
            userAdminConsole.RemoveUserTest();
            userAdminConsole.UpdateUserTest();
            
            teamAdminConsole.CountTeamsTest();
            teamAdminConsole.ListTeamsTest();
            teamAdminConsole.AddTeamTest();
            teamAdminConsole.UpdateTeamTest();
            teamAdminConsole.DeleteTeamTest();

        }
    }
}
