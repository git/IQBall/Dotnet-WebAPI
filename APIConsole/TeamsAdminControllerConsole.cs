﻿using API.Controllers.Admin;
using DbServices;
using Microsoft.Extensions.Logging;
using Services;

namespace APIConsole
{
    public class TeamsAdminControllerConsole
    {
        private TeamsAdminController _controller;

        public TeamsAdminControllerConsole()
        {
            AppContext.AppContext context = new AppContext.AppContext();
            ITeamService teams = new DbTeamService(context);
            ILoggerFactory loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
            ILogger<TeamsAdminController> logger = loggerFactory.CreateLogger<TeamsAdminController>();
            _controller = new TeamsAdminController(teams, logger);
        }

        public async void CountTeamsTest()
        {
            var result = await _controller.CountTeams();
            Console.WriteLine($"CountTeams Result: {result}");
        }
        
        public async void ListTeamsTest()
        {
            var result = await _controller.ListTeams(0, 10);
            Console.WriteLine($"ListTeams Result: {result}");
        }
        
        public async void AddTeamTest()
        {
            var result =
                await _controller.AddTeam(new TeamsAdminController.AddTeamRequest("Lakers", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Los_Angeles_Lakers_logo.svg/2560px-Los_Angeles_Lakers_logo.svg.png", "#FFFFFF", "#000000"));
            Console.WriteLine($"AddTeam Result: {result}");
        }
        
        public async void UpdateTeamTest()
        {
            var result =
                await _controller.UpdateTeam(1, new TeamsAdminController.UpdateTeamRequest("Lakers", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Los_Angeles_Lakers_logo.svg/2560px-Los_Angeles_Lakers_logo.svg.png", "#999999", "#000000"));
            Console.WriteLine($"UpdateTeam Result: {result}");
        }

        public async void DeleteTeamTest()
        {
            var result = await _controller.DeleteTeams(new TeamsAdminController.DeleteTeamsRequest([1]));
            Console.WriteLine($"RemoveTeam Result: {result}");
        }
    }
}