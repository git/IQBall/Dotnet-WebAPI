﻿using System;
using API.Context;
using API.Controllers;
using API.DTO;
using DbServices;
using Model;
using Services;

namespace APIConsole
{
    public class TeamsControllerConsole
    {
        private TeamsController _controller;

        public TeamsControllerConsole()
        {
            AppContext.AppContext context = new AppContext.AppContext();
            ITeamService teams = new DbTeamService(context);
            ITacticService tactics = new DbTacticService(context);
            IUserService users = new DbUserService(context);
            IContextAccessor accessor = new HttpContextAccessor();
            _controller = new TeamsController(teams, tactics, users, accessor);
        }

        public async void GetMembersOfTest()
        {
            var result = await _controller.GetMembersOf(1);
            Console.WriteLine($"GetMembersOf Result: {result}");
        }
        
        public async void CreateTeamTest()
        {
            var result = await _controller.CreateTeam(new TeamsController.CreateTeamRequest("Lakers", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Los_Angeles_Lakers_logo.svg/2560px-Los_Angeles_Lakers_logo.svg.png", "#FFFFFF", "#000000"));
            Console.WriteLine($"CreateTeam Result: {result}");
        }
        
        public async void AddMemberTest()
        {
            var result = await _controller.AddMember(1, new TeamsController.AddMemberRequest(1, "PLAYER"));
            Console.WriteLine($"AddMember Result: {result}");
        }
        
        public async void UpdateMemberTest()
        {
            var result = await _controller.UpdateMember(1, 1, new TeamsController.UpdateMemberRequest("COACH"));
            Console.WriteLine($"UpdateMember Result: {result}");
        }
        
        public async void RemoveMemberTest()
        {
            var result = await _controller.RemoveMember(1, 1);
            Console.WriteLine($"RemoveMember Result: {result}");
        }
    }
}