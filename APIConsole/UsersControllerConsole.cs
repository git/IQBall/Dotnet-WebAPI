﻿using API.Context;
using API.Controllers;
using DbServices;
using Services;

namespace APIConsole
{
    public class UsersControllerConsole
    {
        private UsersController _controller;

        public UsersControllerConsole()
        {
            AppContext.AppContext context = new AppContext.AppContext();
            IUserService users = new DbUserService(context);
            ITeamService teams = new DbTeamService(context);
            ITacticService tactics = new DbTacticService(context);
            IContextAccessor accessor = new HttpContextAccessor();

            _controller = new UsersController(users, teams, tactics, accessor);
        }

        public async void GetUserTest()
        {
            var result = await _controller.GetUser();
            Console.WriteLine($"GetUser Result: {result}");
        }

        public async void GetUserDataTest()
        {
            var result = await _controller.GetUserData();
            Console.WriteLine($"GetUserData Result: {result}");
        }
        
    }
}