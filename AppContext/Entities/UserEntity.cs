using System.ComponentModel.DataAnnotations;

namespace AppContext.Entities;

public class UserEntity
{
    [Key] public int Id { get; set; }

    public required string Password { get; set; }
    public required byte[] PasswordSalt { get; set; }
    public required string Name { get; set; }
    public required string Email { get; set; }
    public required string ProfilePicture { get; set; }
    public required bool IsAdmin { get; set; }

    public ICollection<TacticEntity> Tactics { get; set; } = new List<TacticEntity>();
}