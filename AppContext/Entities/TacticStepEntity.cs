using System.ComponentModel.DataAnnotations;
using Model;

namespace AppContext.Entities;

public class TacticStepEntity
{
    
    public int Id { get; set; }
    

    public int TacticId { get; set; }
    public TacticEntity Tactic { get; set; }


    public required int? ParentId { get; set; }
    public TacticStepEntity? Parent { get; set; }

    [MaxLength(2_000_000)]
    public required string JsonContent { get; set; }
}