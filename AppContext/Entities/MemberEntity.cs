using Model;

namespace AppContext.Entities;

public class MemberEntity
{
    
    public int TeamId { get; set; }
    public TeamEntity? Team { get; set; }

    public required int UserId { get; set; }
    public UserEntity? User { get; set; }

    public required MemberRole Role { get; set; }
}