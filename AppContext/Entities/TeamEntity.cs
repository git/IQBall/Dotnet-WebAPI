namespace AppContext.Entities;

public class TeamEntity
{
    public int Id { get; set; }
    public required string Name { get; set; }
    public required string Picture { get; set; }
    public required string MainColor { get; set; }
    public required string SecondColor { get; set; }

    public ICollection<MemberEntity> Members { get; set; } = new List<MemberEntity>();
}