﻿using System.ComponentModel.DataAnnotations;

namespace AppContext.Entities
{
    public class SharedTacticEntity
    {
        [Key] public int Id { get; set; }
        public int TacticId { get; set; }
        public int? SharedWithUserId { get; set; }
        public int? SharedWithTeamId { get; set; }
    }
}