using Model;

namespace AppContext.Entities;

public class TacticEntity
{
    public int Id { get; init; }
    public required string Name { get; set; }
    public required DateTime CreationDate { get; set; }

    public required int OwnerId { get; set; }
    public UserEntity? Owner { get; set; }

    public CourtType Type { get; set; }
}