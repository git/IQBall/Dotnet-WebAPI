﻿using System.Security.Cryptography;
using AppContext.Entities;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AppContext;

public class AppContext : DbContext
{
    public DbSet<UserEntity> Users { get; init; }
    public DbSet<TacticEntity> Tactics { get; init; }
    public DbSet<TeamEntity> Teams { get; init; }
    public DbSet<MemberEntity> Members { get; init; }
    public DbSet<TacticStepEntity> TacticSteps { get; set; }
    public DbSet<SharedTacticEntity> SharedTactics { get; set; }

    
    public AppContext()
    {
        
    }

    public AppContext(DbContextOptions<AppContext> options) : base(options)
    {
        
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            var pgsqliteDsn = Environment.GetEnvironmentVariable("PGSQL_DSN");

            if (pgsqliteDsn != null)
            {
                optionsBuilder.UseNpgsql(pgsqliteDsn);
            }
            else
            {
                optionsBuilder.UseSqlite("Data Source=database.db");
            }
        }
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.Entity<MemberEntity>()
            .HasKey("UserId", "TeamId");
    }


}