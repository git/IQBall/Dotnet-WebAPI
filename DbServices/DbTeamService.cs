using AppContext.Entities;
using Converters;
using Microsoft.EntityFrameworkCore;
using Model;
using Services;

namespace DbServices;

public class DbTeamService(AppContext.AppContext context) : ITeamService
{
    public Task<IEnumerable<Team>> ListTeamsOf(int userId)
    {
        return Task.FromResult(
            context.Teams
                .Include(t => t.Members)
                .Where(t => t.Members.Any(m => m.UserId == userId))
                .AsEnumerable()
                .Select(t => t.ToModel())
        );
    }


    public Task<IEnumerable<Team>> ListTeams(int start, int count)
    {
        return Task.FromResult(
            context.Teams
                .Skip(start)
                .Take(count)
                .AsEnumerable()
                .Select(e => e.ToModel())
        );
    }


    public async Task<int> CountTotalTeams()
    {
        return await context.Teams.CountAsync();
    }

    public async Task<Team> AddTeam(string name, string picture, string firstColor, string secondColor)
    {
        var entity = new TeamEntity
        {
            Name = name,
            Picture = picture,
            MainColor = firstColor,
            SecondColor = secondColor
        };

        await context.Teams.AddAsync(entity);
        await context.SaveChangesAsync();
        return entity.ToModel();
    }

    public async Task<Team?> GetTeam(int id)
    {
        var entity = await context.Teams.FirstOrDefaultAsync(t => t.Id == id);
        return entity?.ToModel();
    }

    public async Task RemoveTeams(params int[] teams)
    {
        await context.Teams
            .Where(t => teams.Contains(t.Id))
            .ExecuteDeleteAsync();
    }

    public async Task<bool> UpdateTeam(Team team)
    {
        var entity = await context.Teams.FirstOrDefaultAsync(t => t.Id == team.Id);
        if (entity == null)
            return false;

        entity.Name = team.Name;
        entity.MainColor = team.MainColor;
        entity.SecondColor = team.SecondColor;
        entity.Picture = team.Picture;

        return await context.SaveChangesAsync() > 0;
    }

    public async Task<IEnumerable<Tactic>> GetSharedTacticsToTeam(int teamId)
    {
        var sharedTactics = await context.SharedTactics
            .Where(st => st.SharedWithTeamId == teamId)
            .ToListAsync();

        var tactics = new List<Tactic>();
        foreach (var sharedTactic in sharedTactics)
        {
            var tactic = await context.Tactics
                .Where(t => t.Id == sharedTactic.TacticId)
                .Select(t => t.ToModel())
                .FirstOrDefaultAsync();

            if (tactic != null)
            {
                tactics.Add(tactic);
            }
        }

        return tactics;
    }


    public Task<IEnumerable<Member>> GetMembersOf(int teamId)
    {
        return Task.FromResult(context.Members
            .Where(m => m.TeamId == teamId)
            .AsEnumerable()
            .Select(e => e.ToModel()));
    }

    public async Task<Member?> AddMember(int teamId, int userId, MemberRole role)
    {
        if (await context.Members.AnyAsync(m => m.TeamId == teamId && m.UserId == userId))
        {
            return null;
        }

        await context.Members.AddAsync(new MemberEntity
        {
            TeamId = teamId,
            UserId = userId,
            Role = role
        });
        await context.SaveChangesAsync();
        return new Member(teamId, userId, role);
    }

    public async Task<bool> UpdateMember(Member member)
    {
        var entity =
            await context.Members.FirstOrDefaultAsync(e => e.TeamId == member.TeamId && e.UserId == member.UserId);
        if (entity == null)
            return false;
        entity.Role = member.Role;

        return await context.SaveChangesAsync() > 0;
    }

    public async Task<bool> RemoveMember(int teamId, int userId)
    {
        return await context.Members
            .Where(e => e.TeamId == teamId && e.UserId == userId)
            .ExecuteDeleteAsync() > 0;
    }


    public async Task<ITeamService.TeamAccessibility> EnsureAccessibility(int userId, int teamId, MemberRole role)
    {
        var member = await context.Members.FirstOrDefaultAsync(e => e.TeamId == teamId && e.UserId == userId);
        if (member == null)
            return ITeamService.TeamAccessibility.NotFound;

        if (member.Role == role || role == MemberRole.Player)
            return ITeamService.TeamAccessibility.Authorized;

        return role == MemberRole.Coach
            ? ITeamService.TeamAccessibility.Authorized
            : ITeamService.TeamAccessibility.Unauthorized;
    }
    
    public async Task<bool> IsUserInTeam(int userId, int teamId)
    {
        return await context.Members
            .AnyAsync(m => m.TeamId == teamId && m.UserId == userId);
    }
}