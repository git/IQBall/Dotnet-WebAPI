using AppContext.Entities;
using Converters;
using Microsoft.EntityFrameworkCore;
using Model;
using Services;

namespace DbServices;

public class DbTacticService(AppContext.AppContext context) : ITacticService
{
    public Task<IEnumerable<Tactic>> ListTacticsOf(int userId)
    {
        return Task.FromResult(
            context.Tactics
                .Where(t => t.OwnerId == userId)
                .AsEnumerable()
                .Select(e => e.ToModel())
        );
    }

    public async Task<bool> IsOwnerOf(int userId, int tacticId)
    {
        var tacticEntity = await context.Tactics.FirstOrDefaultAsync(u => u.Id == tacticId);
        if (tacticEntity == null)
            return false;

        return tacticEntity.OwnerId == userId;
    }

    public async Task<(int, int)> AddTactic(int userId, string name, CourtType courtType)
    {
        var tacticEntity = new TacticEntity
        {
            Name = name,
            CreationDate = DateTime.Now,
            OwnerId = userId,
            Type = courtType
        };


        var stepEntity = new TacticStepEntity
        {
            ParentId = null,
            JsonContent = "{\"components\": []}",
            Tactic = tacticEntity
        };
        await context.Tactics.AddAsync(tacticEntity);
        await context.TacticSteps.AddAsync(stepEntity);
        await context.SaveChangesAsync();

        
        return (tacticEntity.Id, stepEntity.Id);
    }

    public async Task<bool> UpdateName(int tacticId, string name)
    {
        var entity = await context.Tactics.FirstOrDefaultAsync(t => t.Id == tacticId);
        if (entity == null)
            return false;

        entity.Name = name;
        await context.SaveChangesAsync();
        return true;
    }

    public async Task<bool> SetTacticStepContent(int tacticId, int stepId, string json)
    {
        var entity = await context.TacticSteps
            .FirstOrDefaultAsync(t => t.TacticId == tacticId && t.Id == stepId);
        if (entity == null)
            return false;

        entity.JsonContent = json;
        await context.SaveChangesAsync();
        return true;
    }

    public async Task<string?> GetTacticStepContent(int tacticId, int stepId)
    {
        return (await context
                .TacticSteps
                .FirstOrDefaultAsync(t => t.TacticId == tacticId && t.Id == stepId)
            )?.JsonContent;
    }


    public async Task<TacticStep> GetRootStep(int tacticId)
    {
        return (await context.TacticSteps
                .FirstAsync(e => e.TacticId == tacticId && e.ParentId == null))
            .ToModel(context.TacticSteps);
    }

    public async Task<Tactic?> GetTactic(int tacticId)
    {
        return (await context.Tactics
                .FirstOrDefaultAsync(s => s.Id == tacticId))
            ?.ToModel();
    }

    public Task<IEnumerable<Tactic>> ListUserTactics(int userId)
    {
        return Task.FromResult(context
            .Tactics
            .Where(t => t.OwnerId == userId)
            .AsEnumerable()
            .Select(e => e.ToModel())
        );
    }

    public async Task<int?> AddTacticStep(int tacticId, int parentStepId, string initialJson)
    {

        var parentExists = context.TacticSteps.Any(t => t.TacticId == tacticId && t.Id == parentStepId);
        if (!parentExists)
            return null;
        
        var tactic = await context.Tactics.FirstOrDefaultAsync(t => t.Id == tacticId);
        if (tactic == null)
        {
            return null;
        }

        var entity = new TacticStepEntity
        {
            JsonContent = initialJson,
            ParentId = parentStepId,
            TacticId = tacticId,
        };

        await context.AddAsync(entity);
        await context.SaveChangesAsync();


        return entity.Id;
    }

    public async Task<bool> RemoveTacticStep(int tacticId, int stepId)
    {
        var toRemove = new Stack<TacticStepEntity>();

        var beginStep = await context
            .TacticSteps
            .FirstOrDefaultAsync(t => t.TacticId == tacticId && t.Id == stepId);

        if (beginStep == null)
            return false;

        toRemove.Push(beginStep);

        while (toRemove.TryPop(out var step))
        {
            await context.TacticSteps
                .Where(s => s.TacticId == tacticId && s.ParentId == step.Id)
                .ForEachAsync(toRemove.Push);

            context.Remove(step);
        }

        return await context.SaveChangesAsync() > 0;
    }

    public async Task<bool> ShareTactic(int tacticId, int? userId, int? teamId)
    {
        var sharedTactic = new SharedTacticEntity
        {
            TacticId = tacticId,
            SharedWithUserId = userId,
            SharedWithTeamId = teamId
        };

        await context.SharedTactics.AddAsync(sharedTactic);
        return await context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UnshareTactic(int tacticId, int? userId, int? teamId)
    {
        SharedTacticEntity? sharedTactic = null;
        if (userId.HasValue)
        {
            sharedTactic = await context.SharedTactics
                .FirstOrDefaultAsync(st => st.TacticId == tacticId && st.SharedWithUserId == userId);
        }
        else if (teamId.HasValue)
        {
            sharedTactic = await context.SharedTactics
                .FirstOrDefaultAsync(st => st.TacticId == tacticId && st.SharedWithTeamId == teamId);
        }
        
        if (sharedTactic == null)
        {
            return false;
        }

        context.SharedTactics.Remove(sharedTactic);
        return await context.SaveChangesAsync() > 0;
    }

    public async Task<bool> RemoveTactic(int tacticId)
    {
        var removed = await context.Tactics.Where(t => t.Id == tacticId).ExecuteDeleteAsync() > 0;
        if (!removed)
            return false;

        await context.TacticSteps.Where(s => s.TacticId == tacticId).ExecuteDeleteAsync();
        
        return true;
    }
}