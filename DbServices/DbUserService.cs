﻿using AppContext.Entities;
using Converters;
using Microsoft.EntityFrameworkCore;
using Model;
using Services;
using Services.Failures;


namespace DbServices;

public class DbUserService(AppContext.AppContext context) : IUserService
{
    public Task<int> UsersCount(string nameNeedle)
    {
        return context.Users.CountAsync(u => u.Name.ToLower().Contains(nameNeedle.ToLower()));
    }

    public Task<int> UsersCount()
    {
        return context.Users.CountAsync();
    }

    public Task<IEnumerable<User>> ListUsers(int start, int count, string? nameNeedle = null)
    {
        IQueryable<UserEntity> request = context.Users;

        if (nameNeedle != null)
            request = request.Where(u => u.Name.ToLower().Contains(nameNeedle.ToLower()));

        return Task.FromResult(
            request
                .Skip(start)
                .Take(count)
                .AsEnumerable()
                .Select(e => e.ToModel())
        );
    }

    public async Task<User?> GetUser(int id)
    {
        return (await context.Users.FirstOrDefaultAsync(e => e.Id == id))?.ToModel();
    }

    public async Task<User?> GetUser(string email)
    {
        return (await context.Users.FirstOrDefaultAsync(e => e.Email == email))?.ToModel();
    }

    public async Task<User> CreateUser(
        string username,
        string email,
        string password,
        string profilePicture,
        bool isAdmin
    )
    {
        var (passwordHash, salt) = Hashing.HashString(password);

        var userEntity = new UserEntity
        {
            Name = username,
            Email = email,
            Password = passwordHash,
            PasswordSalt = salt,
            ProfilePicture = profilePicture,
            IsAdmin = isAdmin
        };

        await context.Users.AddAsync(userEntity);
        await context.SaveChangesAsync();

        return userEntity.ToModel();
    }

    public async Task<bool> RemoveUsers(params int[] identifiers)
    {
        return await context
            .Users
            .Where(u => identifiers.Contains(u.Id))
            .ExecuteDeleteAsync() > 0;
    }

    public async Task UpdateUser(User user, string? password = null)
    {
        var entity = await context.Users.FirstOrDefaultAsync(e => e.Id == user.Id);
        if (entity == null)
            throw new ServiceException(Failure.NotFound("User not found"));

        var emailEntity = await context.Users.FirstOrDefaultAsync(e => e.Email == user.Email);
        if (emailEntity != null && emailEntity.Id != entity.Id)
        {
            throw new ServiceException(new Failure("email conflict", "this provided email is used by another account"));
        }
        
        entity.ProfilePicture = user.ProfilePicture;
        entity.Name = user.Name;
        entity.Email = user.Email;
        entity.Id = user.Id;
        entity.IsAdmin = user.IsAdmin;

        if (password != null)
        {
            var (passwordHash, salt) = Hashing.HashString(password);
            entity.Password = passwordHash;
            entity.PasswordSalt = salt;
        }

        await context.SaveChangesAsync();
    }

    public async Task<User?> Authorize(string email, string password)
    {
        var entity = await context
            .Users
            .FirstOrDefaultAsync(u => u.Email == email);

        if (entity == null)
            return null;

        return Hashing.PasswordsMatches(entity.Password, password, entity.PasswordSalt) ? entity.ToModel() : null;
    }

    public async Task<IEnumerable<Tactic>> GetSharedTacticsToUser(int userId)
    {
        var sharedTactics = await context.SharedTactics
            .Where(st => st.SharedWithUserId == userId)
            .ToListAsync();

        var tactics = new List<Tactic>();
        foreach (var sharedTactic in sharedTactics)
        {
            var tactic = await context.Tactics
                .Where(t => t.Id == sharedTactic.TacticId)
                .Select(t => t.ToModel())
                .FirstOrDefaultAsync();

            if (tactic != null)
            {
                tactics.Add(tactic);
            }
        }

        return tactics;
    }
}